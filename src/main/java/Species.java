
public enum Species {
    Dog,
    DomesticCat,
    RoboCat,
    Fish,
    UNKNOWN;
}
