import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class FamilyService {
    private final FamilyDao familyDao = new CollectionFamilyDao();

    public void saveFamily(Family family){
        familyDao.saveFamily(family);
    }
    public List<Family> getAllFamilies(){
        return familyDao.getAllFamilies();
    }
    public void displayAllFamilies(){
        List<Family> familyList = familyDao.getAllFamilies();
        int index = 0;
        for(Family i : familyList) {
            System.out.println("Index: " + index + "\t" + "Family: " + i.toString());
            index++;
        }
    }
    public void getFamiliesBiggerThan(int count){
        List<Family> familyList = familyDao.getAllFamilies();
        int index = 0;
        for(Family i : familyList) {
            if (count < i.countFamily())
                System.out.println("Index: " + index + "\t" + "Family: " + i.toString());
            index++;
        }
    }
    public void getFamiliesLessThan(int count){
        List<Family> familyList = familyDao.getAllFamilies();
        int index = 0;
        for(Family i : familyList) {
            if (count > i.countFamily())
                System.out.println("Index: " + index + "\t" + "Family: " + i.toString());
            index++;
        }
    }
    public void countFamiliesWithMemberNumber(int count){
        List<Family> familyList = familyDao.getAllFamilies();
        int index = 0;
        for(Family i : familyList)
            if (count == i.countFamily())
                index++;
        System.out.println(index);
    }
    public void createNewFamily(Human father, Human mother){
        Family newFamily = new Family(mother, father);
        familyDao.saveFamily(newFamily);
    }
    public void deleteFamilyByIndex(int index){
        familyDao.deleteFamily(index);
    }
    public void bornChild(int index, Human child){
        familyDao.getFamilyByIndex(index).addChild(child);
    }
    public void adoptChild(int index, ArrayList<Human> child){
        familyDao.getFamilyByIndex(index).setChildren(child);
    }
    public void deleteAllChildrenOlderThen(int index, Human child){
        familyDao.getFamilyByIndex(index).deleteChild(child);
    }
    public int count(){
        return familyDao.getAllFamilies().size();
    }
    public Family getFamilyById(int index){
        return familyDao.getFamilyByIndex(index);
    }
    public Pet getPets(){
        List<Family> familyList = familyDao.getAllFamilies();
        //List<Objects> petList = new List<Dog>();
        Pet pet = new Dog();
        for(Family i : familyList) {
            pet = i.getPet();
        }
        return pet;
    }
    public void addPet(int index, Pet pet){
        familyDao.getFamilyByIndex(index).setPet(pet);
    }
}
